package com.apiRest.apirest.controller;

import com.apiRest.apirest.model.Contact;
import com.apiRest.apirest.service.ContactService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContactController {

    private ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    /**
     * Read - Get all employees
     *
     * @return - An Iterable object of Employee full filled
     */
    @GetMapping("/contact")
    public Iterable<Contact> getContact() {
        return contactService.getContact();
    }

}
