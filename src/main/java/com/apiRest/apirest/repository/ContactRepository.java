package com.apiRest.apirest.repository;

import com.apiRest.apirest.model.Contact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ContactRepository extends CrudRepository<Contact, Long> {

}
