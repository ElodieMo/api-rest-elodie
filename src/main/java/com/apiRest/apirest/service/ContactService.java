package com.apiRest.apirest.service;


import com.apiRest.apirest.model.Contact;
import com.apiRest.apirest.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactService {

    @Autowired
    private ContactRepository contactRepository;

    public Optional<Contact> getContact(final Long id) {
        return contactRepository.findById(id);
    }

    public Iterable<Contact> getContact() {
        return contactRepository.findAll();
    }

    public void deleteContact(final Long id) {
        contactRepository.deleteById(id);
    }

    public Contact saveContact(Contact contact) {
        Contact savedContact = contactRepository.save(contact);
        return savedContact;
    }

}
